export interface IRGBA {
  readonly R: number
  readonly G: number
  readonly B: number
  readonly A: number

  AsString(): string
}

export interface IRGBAType {
  new(r: number, g: number, b: number, a: number): IRGBA

  /**
   * Instantiates new Vector2D by stringified value
   */
  FromString(str: string): IRGBA
}
