import { RGBA } from './rgba'

describe('>>> RGB', () => {
  it('should instantiate with provided values', () => {
    const rgba = new RGBA(1, 2, 3, 4)
    expect(rgba.R).toEqual(1)
    expect(rgba.G).toEqual(2)
    expect(rgba.B).toEqual(3)
    expect(rgba.A).toEqual(4)
  })

  it('should convert to string', () => {
    const rgba = new RGBA(1, 2, 3, 4)
    expect(rgba.AsString()).toBe('rgba(1, 2, 3, 4)')
  })

  it('should instantiate from string', () => {
    const rgba = RGBA.FromString('rgba(1, 2, 3, 4)')
    expect(rgba.R).toEqual(1)
    expect(rgba.G).toEqual(2)
    expect(rgba.B).toEqual(3)
    expect(rgba.A).toEqual(4)
  })

  it('should throw an error if cannot instantiate from string', () => {
    expect(() => RGBA.FromString('')).toThrow()
    expect(() => RGBA.FromString('?')).toThrow()
    expect(() => RGBA.FromString('rgba()')).toThrow()
    expect(() => RGBA.FromString('rgba(1)')).toThrow()
    expect(() => RGBA.FromString('rgba(1,2)')).toThrow()
    expect(() => RGBA.FromString('rgba(1,2,3)')).toThrow()
  })
})
