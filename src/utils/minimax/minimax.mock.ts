import {
  IMockMinimaxMove as IMove,
  MockMinimaxTeam as Team,
  IMockMinimaxShip as IShip
} from './minimax.mock.h'
import { IMinimaxBoard } from './minimax.h'
import { IVector2D, Vector2D } from '../vector2D'

export class MockMinimaxShip implements IShip {
  constructor(
    public Index: number,
    public Health: number,
    public MaxHealth: number,
    public Position: IVector2D,
    public Team: Team,
    public Damage: number
  ) { }

  public MakeDamage(amount: number): void {
    this.Health -= amount

    if (this.Health < 0) {
      this.Health = 0
    }
  }
}

export class MockMinimaxBoard implements IMinimaxBoard<Team, IMove> {
  private readonly _girdSize = new Vector2D(3, 3)
  private readonly _relocateDistance = 1
  private readonly _ships: MockMinimaxShip[] = []

  constructor(
    private _teamAShips: MockMinimaxShip[],
    private _teamBShips: MockMinimaxShip[],
    private _currentTeam: Team,
    private _currentShip: MockMinimaxShip
  ) {
    this._ships = _teamAShips.concat(_teamBShips)
  }

  private get TeamAHealth(): number {
    const health = this.CalcShipHealth(this._teamAShips)
    return isNaN(health) ? 0 : health
  }

  private get TeamBHealth(): number {
    const health = this.CalcShipHealth(this._teamBShips)
    return isNaN(health) ? 0 : health
  }

  public get CurrentTeam(): Team {
    return this._currentTeam
  }

  public get IsGameOver(): boolean {
    return this.TeamAHealth === 0 || this.TeamBHealth === 0
  }

  public get Moves(): IMove[] {
    const moves: IMove[] = []

    let minX = this._currentShip.Position.x - this._relocateDistance
    if (minX < 0) {
      minX = 0
    }

    let minY = this._currentShip.Position.y - this._relocateDistance
    if (minY < 0) {
      minY = 0
    }

    let maxX = this._currentShip.Position.x + this._relocateDistance
    if (maxX > this._girdSize.x) {
      maxX = this._girdSize.x
    }

    let maxY = this._currentShip.Position.y + this._relocateDistance
    if (maxY > this._girdSize.y) {
      maxY = this._girdSize.y
    }

    for (let y = minY; y <= maxY; y++) {
      for (let x = minX; x <= maxX; x++) {
        const move: IMove = {
          Relocate: null,
          Attack: null
        }
        const ships = this._ships
        const position = new Vector2D(x, y)
        const shipOnPosition = ships.find(ship => ship.Position.IsEqual(position))

        if (!shipOnPosition) {
          move.Relocate = position
          moves.push(move)
        } else if (shipOnPosition.Team !== this._currentTeam) {
          move.Attack = {
            Target: shipOnPosition,
            Amount: this._currentShip.Damage
          }

          moves.push(move)
        }
      }
    }

    return moves
  }

  public Evaluate(team: Team): number {
    return team === Team.A ? (this.TeamAHealth - this.TeamBHealth) : (this.TeamBHealth - this.TeamAHealth)
  }

  public MakeMove(move: IMove): MockMinimaxBoard {
    const teamAShips: MockMinimaxShip[] = [...this._teamAShips] // clone
    const teamBShips: MockMinimaxShip[] = [...this._teamBShips] // clone

    let currentTeamShips = teamAShips
    let oppositeTeamShips = teamBShips
    if (this._currentTeam === Team.B) {
      currentTeamShips = teamBShips
      oppositeTeamShips = teamAShips
    }

    if (move.Relocate) {
      const ship = currentTeamShips.find(ship => ship.Index === this._currentShip.Index)
      if (!ship) { throw new Error('Where is ship???') }
      ship.Position = move.Relocate
    }

    if (move.Attack) {
      const attack = move.Attack
      const target = oppositeTeamShips.find(ship => ship.Index === attack.Target.Index)
      if (!target) { throw new Error('Where is target ship???') }
      target.MakeDamage(move.Attack.Amount)
    }

    let nextTeam = this._currentTeam
    let nextShip = currentTeamShips[this._currentShip.Index + 1]
    if (!nextShip) { // if there is no next ship it means all ships have done their moves, time to turn over to another team
      nextTeam = (this._currentTeam === Team.A) ? Team.B : Team.A
      nextShip = oppositeTeamShips[0]
    }

    return new MockMinimaxBoard(
      teamAShips,
      teamBShips,
      nextTeam,
      nextShip
    )
  }

  private CalcShipHealth(ships: MockMinimaxShip[]): number {
    const health = { current: 0, max: 0 }
    for (const ship of ships) {
      health.max += ship.MaxHealth
      health.current += ship.Health
    }

    return Math.round((health.current / health.max) * 100)
  }
}


export const mockMinimaxShips = (size = 1, team: Team, startPosition: IVector2D, health = 100, maxHealth = 100, damage = 10): MockMinimaxShip[] => {
  const ships: MockMinimaxShip[] = []

  for (let i = 0; i < size; i++) {
    ships.push(new MockMinimaxShip(
      i,
      health,
      maxHealth,
      new Vector2D(startPosition.x, startPosition.y + i),
      team,
      damage
    ))
  }

  return ships
}
