import { IVector2D } from '@/utils/vector2D'

export interface IMockMinimaxShip {
  Index: number
  Health: number
  MaxHealth: number
  Position: IVector2D
  Team: MockMinimaxTeam
  Damage: number

  MakeDamage(amount: number): void
}

export interface IMockMinimaxMove {
  Relocate: IVector2D | null
  Attack: IMockMinimaxMoveAttack | null
}

export interface IMockMinimaxMoveAttack {
  Amount: number
  Target: IMockMinimaxShip
}

export enum MockMinimaxTeam { A, B }
