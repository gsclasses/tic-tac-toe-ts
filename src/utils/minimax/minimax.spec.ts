import { GetBestMove } from './minimax'
import { MockMinimaxBoard, mockMinimaxShips } from './minimax.mock'
import { IMockMinimaxMove as Move, MockMinimaxTeam as Team } from './minimax.mock.h'
import { Vector2D } from '../vector2D'

describe('>>> GetBestMove', () => {
  it('should try to move towards enemy', () => {
    const teamAShips = mockMinimaxShips(2, Team.A, new Vector2D(0, 0))
    const teamBShips = mockMinimaxShips(2, Team.B, new Vector2D(2, 0))
    const board = new MockMinimaxBoard(teamAShips, teamBShips, Team.A, teamAShips[0])

    expect(GetBestMove<Team, Move>(board, Team.A, 10)).toEqual<Move | null>({
      Attack: null,
      Relocate: new Vector2D(1, 0)
    })
  })

  it('should try to attack if enemy is nearby', () => {
    const teamAShips = mockMinimaxShips(2, Team.A, new Vector2D(0, 0))
    const teamBShips = mockMinimaxShips(2, Team.B, new Vector2D(1, 0))
    const board = new MockMinimaxBoard(teamAShips, teamBShips, Team.A, teamAShips[0])

    expect(GetBestMove<Team, Move>(board, Team.A, 10)).toEqual<Move | null>({
      Attack: {
        Target: teamBShips[0],
        Amount: teamAShips[0].Damage
      },
      Relocate: null
    })
  })
})
