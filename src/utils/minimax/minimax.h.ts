export interface IMinimaxBoard<T, M> {
  readonly IsGameOver: boolean
  readonly CurrentTeam: T
  readonly Moves: M[]
  Evaluate(team: T): number
  MakeMove(move: M): IMinimaxBoard<T, M>
}

export interface IMinimaxDecision<M> {
  score: number
  move: M | null
}
