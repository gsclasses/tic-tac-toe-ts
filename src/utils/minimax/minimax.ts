import { IMinimaxDecision, IMinimaxBoard } from './minimax.h'

const minimax = <T, M>(board: IMinimaxBoard<T, M>, team: T, maxDepth: number, currentDepth: number): IMinimaxDecision<M> => {
  if (board.IsGameOver || currentDepth === maxDepth) {
    return {
      score: board.Evaluate(team),
      move: null
    }
  }

  let bestMove: M | null = null
  let bestScore = board.CurrentTeam === team ? -Infinity : Infinity

  for (const current of board.Moves) {
    const newBoard = board.MakeMove(current)
    const { score } = minimax<T, M>(newBoard, team, maxDepth, currentDepth + 1)

    if (board.CurrentTeam === team) {
      if (score > bestScore) {
        bestScore = score
        bestMove = current
      }
    } else {
      if (score < bestScore) {
        bestScore = score
        bestMove = current
      }
    }
  }

  return {
    score: bestScore,
    move: bestMove
  }
}

export const GetBestMove = <T, M>(board: IMinimaxBoard<T, M>, team: T, maxDepth: number): M | null => {
  const { move } = minimax(board, team, maxDepth, 0)

  return move
}
