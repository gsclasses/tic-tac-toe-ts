import { Component } from './component'
import { IUpdatable } from '../updatable'

export abstract class Entity implements IUpdatable {
  protected components: Component[] = []

  constructor() {
    setTimeout(() => { this.Awake() }, 0)
  }

  public get Components (): Component[] {
    return this.components
  }

  public AddComponent (component: Component): void {
    this.components.push(component)
    component.Entity = this
  }

  public GetComponent<C extends Component>(constr: { new (...args: never): C } | Function ): C {
    for(const component of this.components){
      if(component instanceof constr){
        return component as C
      }
    }

    throw new Error(`Component ${constr.name} not found on Entity ${this.constructor.name}`)
  }

  public HasComponent<C extends Component>(constr: { new (...args: never): C } | Function ): boolean {
    for(const component of this.components){
      if(component instanceof constr){
        return true
      }
    }

    return false
  }

  public RemoveComponent<C extends Component>(constr: { new (...args: never): C } | Function): void {
    const components: Component[] = []
    let toRemove: Component | null = null

    for(const component of this.components){
      if(component instanceof constr){
        toRemove = component
      } else {
        components.push(component)
      }
    }

    if(toRemove){
      toRemove.Entity = null
      this.components = components
    }
  }

  public Awake(): void {
    // Use in children
    this.components.map(component => component.Awake())
  }

  public Update(deltaTime: number): void {
    this.components.map(component => component.Update(deltaTime))
  }
}
