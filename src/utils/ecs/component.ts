import { Entity } from './entity'
import { IUpdatable } from '../updatable'

export abstract class Component implements IUpdatable {
  public Entity: Entity | null

  public Awake(): void {
    // Override in children
  }

  public Update(deltaTime: number): void {
    // Override in children
  }
}

