import { ILerp } from './lerp.h'

export const Lerp: ILerp = (start: number, end: number, t: number): number => {
  return start * (1 - t) + end * t
}
