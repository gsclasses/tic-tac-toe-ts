export type ILerp = (start: number, end: number, t: number) => number
