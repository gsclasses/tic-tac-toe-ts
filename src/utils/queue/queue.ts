import { IQueue } from './queue.h'

export class Queue<T> implements IQueue<T> {
  private items: T[] = []

  get Items (): T[] {
    return this.items
  }

  get IsEmpty (): boolean {
    return this.items.length === 0
  }

  Enqueue(elm: T): void {
    this.items.push(elm)
  }

  Dequeue(): T {
    const item = this.items.shift()
    if(typeof item === 'undefined'){
      throw new Error('Attempt to dequeue from an empty queue')
    }

    return item
  }
}
