export interface IQueue<T> {
  /**
   * List of all items in the Queue
   */
  readonly Items: T[]

  /**
   * Determines whether Queue is empty
   */
  readonly IsEmpty: boolean

  /**
   * Add new element to the end of the Queue
   */
  Enqueue(elm: T): void

  /**
   * Get first element from the Queue
   */
  Dequeue(): T
}
