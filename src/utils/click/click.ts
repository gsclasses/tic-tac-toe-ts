import { IVector2D, Component } from '@/utils'

export abstract class ClickComponent extends Component {
  public abstract Click(on: IVector2D): void
}
