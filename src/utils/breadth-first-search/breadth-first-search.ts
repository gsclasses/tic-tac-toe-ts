import { IGraph, IBreadthFirstSearch } from './breadth-first-search.h'
import { Queue } from '@/utils/queue'
import { IVector2D } from '@/utils/vector2D'
import { Dictionary } from '@/utils/dictionary'

export const BreadthFirstSearch: IBreadthFirstSearch = (graph: IGraph, start: IVector2D): Dictionary<IVector2D | null> => {
  const frontier = new Queue<IVector2D>()
  const path: Dictionary<IVector2D | null> = {
    [start.AsString()]: null
  }

  frontier.Enqueue(start)

  while (!frontier.IsEmpty) {
    const current = frontier.Dequeue()
    for (const next of graph.GetNeighborsForNodeWithIndex(current)) {
      if (!Object.prototype.hasOwnProperty.call(path, next.AsString())) {
        frontier.Enqueue(next)
        path[next.AsString()] = current
      }
    }
  }

  return path
}
