import { IVector2D } from '@/utils/vector2D'
import { Dictionary } from '@/utils/dictionary'

export interface IGraph {
  GetNeighborsForNodeWithIndex(index: IVector2D): IVector2D[]
}

export type IBreadthFirstSearch = (graph: IGraph, start: IVector2D) => Dictionary<IVector2D | null>
