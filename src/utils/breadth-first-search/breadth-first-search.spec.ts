import { IGraph } from './breadth-first-search.h'
import { BreadthFirstSearch } from './breadth-first-search'
import { IVector2D, Vector2D } from '@/utils/vector2D'
import { Dictionary } from '@/utils/dictionary'

describe('>>> Breadth First Search', () => {
  class Graph implements IGraph {
    readonly Edges: Dictionary<IVector2D[] | null>

    constructor(edges: Dictionary<IVector2D[] | null>) {
      this.Edges = edges
    }

    public GetNeighborsForNodeWithIndex(node: IVector2D): IVector2D[] {
      const neighbors = this.Edges[node.AsString()]
      if (!neighbors) {
        throw new Error('No neighbors')
      }

      return neighbors
    }
  }

  it('should return path to the desired point', () => {
    const a = new Vector2D(0, 0)
    const b = new Vector2D(1, 0)
    const c = new Vector2D(0, 1)
    const d = new Vector2D(1, 1)

    const data = {
      [a.AsString()]: [b, c],
      [b.AsString()]: [a, d],
      [c.AsString()]: [a, d],
      [d.AsString()]: [b, c]
    }

    expect(BreadthFirstSearch(new Graph(data), a)).toStrictEqual({
      '(0,0)': null,
      '(0,1)': a,
      '(1,0)': a,
      '(1,1)': b
    })

    expect(BreadthFirstSearch(new Graph(data), b)).toStrictEqual({
      '(0,0)': b,
      '(0,1)': a,
      '(1,0)': null,
      '(1,1)': b
    })
  })
})
