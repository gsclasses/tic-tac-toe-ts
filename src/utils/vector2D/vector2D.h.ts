export interface IVector2D {
  x: number
  y: number

  /**
   * Returns stringified representation of the coordinates of this point
   */
  AsString(): string

  /**
   * Determines if two vectors are equal
   */
  IsEqual(other: IVector2D): boolean
}

export interface IVector2DType {
  new(x: number, y: number): IVector2D

  /**
   * Instantiates new Vector2D by stringified value
   */
  GetFromString(str: string): IVector2D

  /**
   * Interpolates between 2 vectors
   */
  Lerp(start: IVector2D, end: IVector2D, t: number): IVector2D
}
