import { IVector2D, IVector2DType } from './vector2D.h'
import { Lerp } from '@/utils/lerp'

export const Vector2D: IVector2DType = class implements IVector2D {
  constructor(public x: number, public y: number) { }

  public AsString(): string {
    return `(${this.x},${this.y})`
  }

  public IsEqual(other: IVector2D): boolean {
    if (this.x === other.x && this.y === other.y) {
      return true
    }

    return false
  }

  public static GetFromString(str: string): IVector2D {
    const parsed = str.replace(new RegExp(/\(|\)/, 'g'), '').split(',')
    const x = Number(parsed[0])
    const y = Number(parsed[1])

    if (isNaN(x) || isNaN(y)) {
      throw new Error(`Cannot instantiate Vector2D from string ${str}`)
    }

    return new Vector2D(x, y)
  }

  public static Lerp(start: IVector2D, end: IVector2D, t: number): IVector2D {
    return new Vector2D(Lerp(start.x, end.x, t), Lerp(start.y, end.y, t))
  }
}
