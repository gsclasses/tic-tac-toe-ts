import { IVector2D } from '../vector2D'
import { Component } from '../ecs'

export abstract class HoverComponent extends Component {
  public abstract Hover(on: IVector2D): void
}
