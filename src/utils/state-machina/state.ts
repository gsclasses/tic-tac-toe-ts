import { IUpdatable } from '../updatable'
import { Settings } from '@/settings'

export abstract class State implements IUpdatable {
  public Next: State[] | null = null

  constructor(next?: State[] | null) {
    if (typeof next !== 'undefined') {
      this.Next = next
    }
  }

  public OnEnter(): void {
    if (Settings.debugMode) {
      console.info('%c%s', 'color: blue;font-weight:900', 'Enter', `${this.constructor.name}`)
    }

    // Call in children
  }

  public OnExit(): void {
    if (Settings.debugMode) {
      console.info(`Exit ${this.constructor.name}`)
    }
    // Call in children
  }

  public Update(deltaTime: number): void {
    // Call in children
  }
}
