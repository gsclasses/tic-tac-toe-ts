import { StateMachina } from './state-machina'
import { State } from './state'

describe('>>> State Machina', () => {
  class StateC extends State {}
  class StateB extends State {}
  class StateA extends State {}

  let stateC: StateC
  let stateB: StateB
  let stateA: StateA
  let sm: SM

  class SM extends StateMachina {
    protected states: State[] = [stateA, stateB, stateC]
    protected current: State = stateA
  }

  beforeEach(() => {
    stateC = new StateC(null)
    stateB = new StateB([stateC])
    stateA = new StateA([stateB])

    sm = new SM()
    sm.Start()
  })

  it('should enter valid state when requested', () => {
    expect(sm.Current == stateA)
    expect(sm.CanEnter(stateB)).toBeTruthy()
    sm.Enter(stateB)
    expect(sm.Current == stateB)

    expect(sm.CanEnter(stateC)).toBeTruthy()
    sm.Enter(stateC)
    expect(sm.Current == stateC)
  })

  it('should throw an error if state can not be entered', () => {
    expect(sm.Current == stateA)
    expect(sm.CanEnter(stateC)).toBeFalsy()

    expect(() => { sm.Enter(stateC) }).toThrow()
  })

  it('should call Update of the current state', () => {
    const spyStateAUpdate = jest.spyOn(stateA, 'Update')
    expect(sm.Current == stateA)
    expect(spyStateAUpdate).not.toBeCalled()
    sm.Update(17)
    expect(spyStateAUpdate).toBeCalled()

    const spyStateBUpdate = jest.spyOn(stateB, 'Update')
    sm.Enter(stateB)
    expect(sm.Current == stateB)
    expect(spyStateBUpdate).not.toBeCalled()
    sm.Update(17)
    expect(spyStateBUpdate).toBeCalled()
  })
})
