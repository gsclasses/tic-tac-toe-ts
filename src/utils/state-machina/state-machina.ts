// import { State } from './state'
import { IUpdatable } from '../updatable'
import { State } from './state'
import { Settings } from '@/settings'

export abstract class StateMachina implements IUpdatable {
  /**
   * All states this Machine operates one
   */
  protected readonly states: State[] = []

  /**
   * Current State
   */
  protected current: State

  /**
   * Is the machina started?
   */
  protected isStarted = false

  /**
   * Returns Current State
   */
  public get Current(): State {
    return this.current
  }

  /**
   * Returns All States
   */
  public get States(): State[] {
    return this.states
  }

  /**
   * isStarted getter
   */
  public get IsStarted(): boolean {
    return this.isStarted
  }

  /**
   * Enters provided State if valid
   */
  public Enter(state: State): void {
    if (!this.CanEnter(state)) {
      throw new Error(`Machina cannot enter this state: ${state.constructor.name} being in ${this.current.constructor.name}`)
    }

    this.current.OnExit()
    this.current = state
    this.current.OnEnter()
  }

  /**
   * Validates if provided State is valid to enter
   */
  public CanEnter(state: State): boolean {
    if (!this.isStarted) {
      return false
    }

    if (!this.current.Next || !this.current.Next.includes(state)) {
      return false
    }

    return true
  }

  public Start(): void {
    if (!this.current) {
      throw new Error(`Cannot start state machine ${this.constructor.name} until current state is set`)
    }

    if (this.states.length < 1) {
      throw new Error(`Cannot start state machine ${this.constructor.name} until at least one state is set`)
    }

    if (Settings.debugMode) {
      console.info('%c%s', 'color: gray;font-weight:900', 'Start', `${this.constructor.name}`)
    }

    this.isStarted = true
    this.current.OnEnter()
  }

  public Stop(): void {
    if (!this.isStarted) {
      console.warn(`Machina ${this.constructor.name} is not running, stopping skipped`)
      return
    }

    if (Settings.debugMode) {
      console.info(`Stop ${this.constructor.name}`)
    }

    this.isStarted = false
  }

  /**
   * Calls update on current state
   */
  public Update(deltaTime: number): void {
    if (!this.isStarted) {
      return
    }

    this.current.Update(deltaTime)
  }
}
