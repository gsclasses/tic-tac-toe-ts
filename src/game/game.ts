import { Grid } from '@/grid'
import { Entity } from '@/utils'
import { ShipFactory } from '@/ships'
import { Team } from './team'
import { GameStateMachina } from './state'
import {
  GameClickComponent,
  GameHoverComponent,
  GameModalStartComponent,
  GameModalLoseComponent,
  GameModalWinComponent
} from './components'
import { Canvas, CanvasLayer } from './canvas'

export class Game extends Entity {
  /**
   * List of Entities
   */
  public Entities: Entity[] = []

  /**
   * Store information about the last time update was executed
   */
  private lastTimestamp = 0
  private stateMachina: GameStateMachina
  private bTeamFactory: ShipFactory
  private aTeamFactory: ShipFactory
  private grid: Grid

  public get Grid(): Grid {
    return this.grid
  }

  public get ATeamFactory(): ShipFactory {
    return this.aTeamFactory
  }

  public get BTeamFactory(): ShipFactory {
    return this.bTeamFactory
  }

  public get StateMachina(): GameStateMachina {
    return this.stateMachina
  }

  /**
   * Executed when game first starts
   */
  public Awake(): void {
    this.grid = new Grid()
    this.bTeamFactory = new ShipFactory(Team.B, this.grid)
    this.aTeamFactory = new ShipFactory(Team.A, this.grid)

    this.aTeamFactory.Opposite = this.bTeamFactory
    this.bTeamFactory.Opposite = this.aTeamFactory

    this.Entities.push(this.grid, this.bTeamFactory, this.aTeamFactory)

    this.AddComponent(new GameClickComponent())
    this.AddComponent(new GameHoverComponent())
    this.AddComponent(new GameModalLoseComponent())
    this.AddComponent(new GameModalWinComponent())
    this.AddComponent(new GameModalStartComponent())

    // Make sure Update starts after All entities are awaken
    setTimeout(() => { window.requestAnimationFrame(() => this.Update()) }, 0)

    this.stateMachina = new GameStateMachina(this)

    super.Awake()
  }

  /**
   * Game Loop
   */
  public Update(): void {
    const deltaTime = (Date.now() - this.lastTimestamp) / 1000
    if (this.lastTimestamp) {
      this.Entities.map(entity => entity.Update(deltaTime))
    }

    if (this.stateMachina.IsStarted) {
      this.stateMachina.Update(deltaTime)
    } else if (this.aTeamFactory.IsReady && this.bTeamFactory.IsReady) {
      this.stateMachina.Start()
    }

    this.lastTimestamp = Date.now()

    window.requestAnimationFrame(() => this.Update())
  }

  public ShowStartModal(): void {
    this.GetComponent(GameModalStartComponent).Show()
  }

  public ShowWinModal(): void {
    this.GetComponent(GameModalWinComponent).Show()
  }

  public ShowLoseModal(): void {
    this.GetComponent(GameModalLoseComponent).Show()
  }

  public HideAllModals(): void {
    this.GetComponent(GameModalStartComponent).Hide()
    this.GetComponent(GameModalLoseComponent).Hide()
    this.GetComponent(GameModalWinComponent).Hide()
  }

  public Clear(): void {
    this.Grid.Clear()

    this.ATeamFactory.RecycleAll()
    this.BTeamFactory.RecycleAll()

    Canvas.Clear(CanvasLayer.Foreground)
    Canvas.Clear(CanvasLayer.Middle)
    Canvas.Clear(CanvasLayer.Background)
  }

  public Start(): void {
    this.stateMachina.Enter(this.stateMachina.StartedState)
  }
}
