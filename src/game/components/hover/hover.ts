import { Component, HoverComponent } from '@/utils'
import { Game } from '../../game'
import { Canvas, CanvasLayer } from '../../canvas'

export class GameHoverComponent extends Component {
  public Entity: Game

  public Awake(): void {
    document.body.addEventListener('mousemove', (e: MouseEvent) => {
      this.handle(e)
    })
  }

  private handle(e: MouseEvent): void {
    const sm = this.Entity.StateMachina
    if(sm.Current !== sm.BTurnState && sm.Current !== sm.ATurnState){
      return
    }

    const point = Canvas.GetPoint(Canvas.GetInstance(CanvasLayer.Background), e)
    for (const entity of this.Entity.Entities) {
      if (entity.HasComponent(HoverComponent)) {
        entity.GetComponent(HoverComponent).Hover(point)
      }
    }
  }
}
