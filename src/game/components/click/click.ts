import { Component, ClickComponent } from '@/utils'
import { Canvas, CanvasLayer } from '../../canvas'
import { Game } from '../../game'

export class GameClickComponent extends Component {
  public Entity: Game

  public Awake(): void {
    document.body.addEventListener('click', (e: MouseEvent) => {
      this.handle(e)
    })
  }

  /**
   * Handle Mouse Click by invoking point of click to all ClickComponents
   */
  private handle(e: MouseEvent): void {
    const sm = this.Entity.StateMachina
    if(sm.Current !== sm.BTurnState && sm.Current !== sm.ATurnState){
      return
    }

    const point = Canvas.GetPoint(Canvas.GetInstance(CanvasLayer.Background), e)
    for (const entity of this.Entity.Entities) {
      if (entity.HasComponent(ClickComponent)) {
        entity.GetComponent(ClickComponent).Click(point)
      }
    }
  }
}
