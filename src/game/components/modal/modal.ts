import { Component } from '@/utils'
import { Game } from '@/game/game'

export abstract class GameModalComponent extends Component {
  public Entity: Game

  protected abstract get modal(): HTMLDivElement

  protected get overlay(): HTMLDivElement {
    const elm = document.querySelector<HTMLDivElement>('.overlay')
    if (!elm) {
      throw new Error('Overlay not found')
    }

    return elm
  }

  public Show(): void {
    const button = this.modal.querySelector('button')
    if (!button) {
      throw new Error('Button not found within modal')
    }

    button.addEventListener('click', () => { this.Entity.Start() }, { once: true })
    this.modal.style.display = 'block'

    this.overlay.style.display = 'block'
  }

  public Hide(): void {
    this.modal.style.display = 'none'
    this.overlay.style.display = 'none'
  }
}
