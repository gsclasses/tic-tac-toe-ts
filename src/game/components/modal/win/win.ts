import { GameModalComponent } from '../modal'

export class GameModalWinComponent extends GameModalComponent {
  protected get modal(): HTMLDivElement {
    const elm = document.querySelector<HTMLDivElement>('.modal.green')
    if (!elm) {
      throw new Error('Win modal window not found')
    }

    return elm
  }
}
