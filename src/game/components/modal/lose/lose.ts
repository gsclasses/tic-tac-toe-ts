import { GameModalComponent } from '../modal'

export class GameModalLoseComponent extends GameModalComponent {
  protected get modal(): HTMLDivElement {
    const elm = document.querySelector<HTMLDivElement>('.modal.red')
    if (!elm) {
      throw new Error('Lose modal window not found')
    }

    return elm
  }
}
