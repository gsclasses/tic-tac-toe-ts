import { GameModalComponent } from '../modal'

export class GameModalStartComponent extends GameModalComponent {
  protected get modal(): HTMLDivElement {
    const elm = document.querySelector<HTMLDivElement>('.modal.blue')
    if (!elm) {
      throw new Error('Start modal window not found')
    }

    return elm
  }

  public Show(): void {
    const button = this.modal.querySelector('button')
    if (!button) {
      throw new Error('Button not found within modal')
    }

    button.addEventListener('click', () => { this.Entity.StateMachina.Enter(this.Entity.StateMachina.BTurnState) }, { once: true })
    this.modal.style.display = 'block'
    this.overlay.style.display = 'block'
  }
}
