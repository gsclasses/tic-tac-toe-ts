import { Settings } from '@/settings/settings'
import { IVector2D, Vector2D } from '@/utils'

export enum CanvasLayer {
  Background,
  Foreground,
  Middle
}

export class Canvas {
  private static background: HTMLCanvasElement | null
  private static foreground: HTMLCanvasElement | null
  private static middle: HTMLCanvasElement | null

  private static ctxBackground: CanvasRenderingContext2D | null
  private static ctxForeground: CanvasRenderingContext2D | null
  private static ctxMiddle: CanvasRenderingContext2D | null

  private constructor() {
    // make it unaccessible
  }

  public static GetInstance(type: CanvasLayer): HTMLCanvasElement {
    switch (type) {
      case CanvasLayer.Foreground: return this.getForegroundInstance()
      case CanvasLayer.Background: return this.getBackgroundInstance()
      case CanvasLayer.Middle: return this.getMiddleInstance()
      default: throw new Error('Canvas type is not supported')
    }
  }

  public static GetCtx(type: CanvasLayer): CanvasRenderingContext2D {
    switch (type) {
      case CanvasLayer.Foreground: return this.getForegroundCtx()
      case CanvasLayer.Background: return this.getBackgroundCtx()
      case CanvasLayer.Middle: return this.getMiddleCtx()
      default: throw new Error('Canvas type is not supported')
    }
  }

  public static GetPoint(canvas: HTMLCanvasElement, e: MouseEvent): IVector2D {
    const canvasRect = canvas.getBoundingClientRect()
    const scrollLeft = window.pageXOffset || document.documentElement.scrollLeft
    const scrollTop = window.pageYOffset || document.documentElement.scrollTop

    const offset = {
      top: canvasRect.top + scrollTop,
      left: canvasRect.left + scrollLeft
    }

    return new Vector2D(
      e.pageX - offset.left,
      e.pageY - offset.top
    )
  }

  public static Clear(type: CanvasLayer): void {
    Canvas.GetCtx(type).clearRect(0, 0, Canvas.GetInstance(type).width, Canvas.GetInstance(type).height)
  }

  private static getBackgroundInstance(): HTMLCanvasElement {
    if (!this.background) {
      this.background = this.createCanvas('background')
    }

    return this.background
  }

  private static getForegroundInstance(): HTMLCanvasElement {
    if (!this.foreground) {
      this.foreground = this.createCanvas('foreground')
    }

    return this.foreground
  }

  private static getMiddleInstance(): HTMLCanvasElement {
    if (!this.middle) {
      this.middle = this.createCanvas('middle')
    }

    return this.middle
  }

  private static createCanvas(id: string): HTMLCanvasElement {
    const canvasSize = this.getCanvasSize()
    const canvas = document.createElement('canvas')
    canvas.setAttribute('width', canvasSize.toString())
    canvas.setAttribute('height', canvasSize.toString())
    canvas.setAttribute('id', id)

    document.body.appendChild(canvas)

    return canvas
  }

  private static getCanvasSize(): number {
    return (Settings.grid.nodeSize + Settings.grid.nodeOffset) * Settings.grid.dimension + Settings.grid.nodeOffset
  }

  private static getBackgroundCtx(): CanvasRenderingContext2D {
    if (!this.ctxBackground) {
      this.ctxBackground = this.GetInstance(CanvasLayer.Background).getContext('2d')
    }

    if (!this.ctxBackground) {
      throw new Error('No Context for old man')
    }

    return this.ctxBackground
  }

  private static getForegroundCtx(): CanvasRenderingContext2D {
    if (!this.ctxForeground) {
      this.ctxForeground = this.GetInstance(CanvasLayer.Foreground).getContext('2d')
    }

    if (!this.ctxForeground) {
      throw new Error('No Context for old man')
    }

    return this.ctxForeground
  }

  private static getMiddleCtx(): CanvasRenderingContext2D {
    if (!this.ctxMiddle) {
      this.ctxMiddle = this.GetInstance(CanvasLayer.Middle).getContext('2d')
    }

    if (!this.ctxMiddle) {
      throw new Error('No Context for old man')
    }

    return this.ctxMiddle
  }

}
