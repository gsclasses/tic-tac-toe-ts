import { GameState } from './game.state'
import { ShipFactory } from '@/ships'
import { GameStateMachina } from './game.state-machina'

export class GameStateBTurn extends GameState {
  constructor(
    private readonly machina: GameStateMachina,
    private readonly shipFactory: ShipFactory,
    next?: GameState[] | null
  ) {
    super(next)
  }

  public OnEnter(): void {
    super.OnEnter()
    this.shipFactory.IsCurrent = true
    this.shipFactory.ActivateNext()
  }

  public OnExit(): void {
    super.OnExit()
    this.shipFactory.IsCurrent = false
  }

  public Update(deltaTime: number): void {
    super.Update(deltaTime)

    if(!this.shipFactory.IsLastOneActivated){
      return
    }

    const currentShip = this.shipFactory.Current
    if(currentShip && currentShip.StateMachina.Current === currentShip.StateMachina.IdleState){
      this.machina.Enter(this.machina.ATurnState)
    }
  }
}
