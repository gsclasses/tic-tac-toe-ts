import { GameState } from './game.state'
import { Game } from '../game'

export class GameStateLose extends GameState {
  constructor(
    private readonly game: Game,
    next?: GameState[] | null
  ) {
    super(next)
  }

  public OnEnter(): void {
    super.OnEnter()
    this.game.ShowLoseModal()
    this.game.Clear()
  }

  public OnExit(): void {
    super.OnExit()
    this.game.HideAllModals()
  }
}
