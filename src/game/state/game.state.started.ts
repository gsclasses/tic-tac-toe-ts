import { GameState } from './game.state'
import { GameStateBTurn } from './game.state.b-turn'
import { GameStateATurn } from './game.state.a-turn'
import { Game } from '../game'

export class GameStateStarted extends GameState {
  public Next: [GameStateBTurn, GameStateATurn]

  constructor(
    private readonly game: Game,
    next?: GameState[] | null
  ) {
    super(next)
  }

  public OnEnter(): void {
    super.OnEnter()
    this.game.ShowStartModal()
    this.game.ATeamFactory.Spawn()
    this.game.BTeamFactory.Spawn()
  }

  public OnExit(): void {
    super.OnExit()
    this.game.HideAllModals()
  }
}
