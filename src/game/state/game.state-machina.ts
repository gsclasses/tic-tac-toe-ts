import { StateMachina } from '@/utils'
import { GameState } from './game.state'
import { GameStateStarted } from './game.state.started'
import { GameStateBTurn } from './game.state.b-turn'
import { GameStateATurn } from './game.state.a-turn'
import { GameStateWin } from './game.state.win'
import { GameStateLose } from './game.state.lose'
import { Game } from '../game'

export class GameStateMachina extends StateMachina {
  protected states: GameState[] = []
  protected current: GameState

  public readonly StartedState: GameStateStarted
  public readonly BTurnState: GameStateBTurn
  public readonly ATurnState: GameStateATurn
  public readonly WinState: GameStateWin
  public readonly LoseState: GameStateLose

  constructor(private readonly game: Game) {
    super()

    this.StartedState = new GameStateStarted(game)
    this.BTurnState = new GameStateBTurn(this, this.game.BTeamFactory)
    this.ATurnState = new GameStateATurn(this, this.game.ATeamFactory)
    this.WinState = new GameStateWin(game)
    this.LoseState = new GameStateLose(game)

    this.StartedState.Next = [this.BTurnState, this.ATurnState]
    this.BTurnState.Next = [this.ATurnState, this.WinState, this.LoseState]
    this.ATurnState.Next = [this.BTurnState, this.WinState, this.LoseState]
    this.WinState.Next = [this.StartedState]
    this.LoseState.Next = [this.StartedState]

    this.states = [
      this.StartedState,
      this.BTurnState,
      this.ATurnState,
      this.WinState,
      this.LoseState
    ]

    this.current = this.StartedState
  }

  public Update(deltaTime: number): void {
    super.Update(deltaTime)

    if(this.Current === this.StartedState){
      return
    }

    if(this.current === this.LoseState || this.current === this.WinState){
      return
    }

    if(this.game.ATeamFactory.Ships.length <= 0){
      this.Enter(this.WinState)
    } else if(this.game.BTeamFactory.Ships.length <= 0) {
      this.Enter(this.LoseState)
    }
  }
}
