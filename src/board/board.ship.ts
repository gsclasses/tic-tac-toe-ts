import { IVector2D, IRange } from '@/utils'
import { Team as T } from '../game/team'

export class BoardShip {
  constructor(
    public readonly Index: number,
    public readonly MaxHealth: number,
    public readonly Team: T,
    public readonly Damage: IRange,
    public Health: number,
    public Position: IVector2D
  ) { }

  public MakeDamage(amount: number): void {
    this.Health -= amount

    if (this.Health < 0) {
      this.Health = 0
    }
  }

  public Clone(): BoardShip {
    return new BoardShip(
      this.Index,
      this.MaxHealth,
      this.Team,
      this.Damage,
      this.Health,
      this.Position
    )
  }
}
