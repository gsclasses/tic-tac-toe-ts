import { IVector2D } from '@/utils'
import { BoardShip } from './board.ship'

export interface IBoardMove {
  Relocate: IVector2D | null
  Attack: IBoardMoveAttack | null
}

export interface IBoardMoveAttack {
  Amount: number
  Target: BoardShip
}
