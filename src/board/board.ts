import { IMinimaxBoard } from '@/utils'
import { Team } from '../game/team'
import { IBoardMove as Move } from './board.h'
import { Settings } from '@/settings'
import { Grid, GridNode } from '@/grid'
import { BoardShip } from './board.ship'
import { ShipFactory } from '@/ships'

export class Board implements IMinimaxBoard<Team, Move> {
  public static ConvertFactoryShipsToBoardShips(factory: ShipFactory): BoardShip[] {
    return factory.Ships.map(ship => {
      const node = ship.Node
      if (!node) {
        throw new Error('Every ship must be standing somewhere')
      }

      return new BoardShip(
        ship.Index,
        ship.Health.Max,
        ship.Factory.Team,
        ship.DamageRange,
        ship.Health.Current,
        node.Index
      )
    })
  }

  private readonly _ships: BoardShip[] = []

  constructor(
    private readonly _teamAShips: BoardShip[],
    private readonly _teamBShips: BoardShip[],
    private readonly _currentTeam: Team,
    private readonly _grid: Grid,
    private readonly _currentShip: BoardShip,
  ) {
    this._ships = _teamAShips.concat(_teamBShips)
  }

  private get TeamAHealth(): number {
    return this.CalcShipHealth(this._teamAShips)
  }

  private get TeamBHealth(): number {
    return this.CalcShipHealth(this._teamBShips)
  }

  public get IsGameOver(): boolean {
    return this.TeamAHealth === 0 || this.TeamBHealth === 0
  }

  public get CurrentTeam(): Team {
    return this._currentTeam
  }

  public get Moves(): Move[] {
    const moves: Move[] = []

    const node = this._grid.GetNodeByIndex(this._currentShip.Position)
    if (!node) {
      throw new Error('???')
    }

    this.FindFlightMoves(moves, node, Settings.ships.flightRange)
    this.FindAttackMoves(moves, node, Settings.ships.flightRange)

    return moves
  }

  public Evaluate(team: Team): number {
    return team === Team.A ? (this.TeamAHealth - this.TeamBHealth) : (this.TeamBHealth - this.TeamAHealth)
  }

  public MakeMove(move: Move): Board {
    if (Settings.debugMode && move.Attack) {
      this.DebugCurrentState('Before Move')
      this.DebugMove(move)
    }

    const teamAShips = this._teamAShips.map(ship => ship.Clone())
    const teamBShips = this._teamBShips.map(ship => ship.Clone())

    let currentTeamShips = teamAShips
    let oppositeTeamShips = teamBShips
    if (this._currentTeam === Team.B) {
      currentTeamShips = teamBShips
      oppositeTeamShips = teamAShips
    }

    let nextTeam = this._currentTeam
    let nextShip = currentTeamShips[this._currentShip.Index + 1]
    if (!nextShip) { // if there is no next ship it means all ships have done their moves, time to turn over to another team
      nextTeam = (this._currentTeam === Team.A) ? Team.B : Team.A
      nextShip = oppositeTeamShips[0]
    }

    const board = new Board(
      teamAShips,
      teamBShips,
      nextTeam,
      this._grid,
      nextShip
    )

    if (move.Relocate) {
      const ship = currentTeamShips.find(ship => ship.Index === this._currentShip.Index)
      if (!ship) { throw new Error('Where is ship???') }
      ship.Position = move.Relocate
    }

    if (move.Attack) {
      const attack = move.Attack
      const target = oppositeTeamShips.find(ship => ship.Index === attack.Target.Index)
      if (!target) { throw new Error('Where is target ship???') }
      target.MakeDamage(move.Attack.Amount)
    }

    return board
  }

  private FindFlightMoves(moves: Move[], node: GridNode, range: number): void {
    const ship = this._ships.find(ship => ship.Position.IsEqual(node.Index))
    if (!ship && !moves.some(move => move.Relocate && move.Relocate.IsEqual(node.Index))) {
      moves.push({
        Relocate: node.Index,
        Attack: null
      })
    }

    const newRange = --range
    if (newRange <= 0) {
      return
    }

    for (const neighbor of node.Neighbors) {
      if (!this._ships.some(ship => ship.Position.IsEqual(neighbor.Index))) {
        this.FindFlightMoves(moves, neighbor, range)
      }
    }
  }

  private FindAttackMoves(moves: Move[], node: GridNode, range: number): void {
    const ship = this._ships.find(ship => ship.Position.IsEqual(node.Index))
    if (ship && ship.Team !== this._currentTeam) {
      moves.push({
        Relocate: null,
        Attack: {
          Target: ship,
          Amount: ship.Damage.to
        }
      })
    }

    const newRange = --range
    if (newRange <= 0) {
      return
    }

    for (const neighbor of node.Neighbors) {
      this.FindAttackMoves(moves, neighbor, range)
    }
  }

  private CalcShipHealth(ships: BoardShip[]): number {
    const health = { current: 0, max: 0 }
    for (const ship of ships) {
      health.max += ship.MaxHealth
      health.current += ship.Health
    }

    const percent = Math.round((health.current / health.max) * 100)

    return isNaN(percent) ? 0 : percent
  }

  private DebugMove(move: Move): void {
    console.log('%c%s', 'color: green;font-weight:900', 'Move',
      `\n Current Ship:
      \t Index: ${this._currentShip.Index},
      \t Team: ${this._currentShip.Team},
      \t Position: ${this._currentShip.Position.AsString()}
      Relocate: ${move.Relocate ? move.Relocate.AsString() : null}
      Attack: ${move.Attack ? `Target: ${move.Attack.Target.Index}, Amount: ${move.Attack.Amount}` : null}
    `)
  }
  private DebugCurrentState(text: string): void {
    const positions = this._ships
      .map(ship => `\t Team: ${ship.Team}, Index: ${ship.Index}, Position: ${ship.Position.AsString()}, Health: ${ship.Health}/${ship.MaxHealth}`)
      .join('\n')

    console.info('%c%s', 'color: red;font-weight:900', `Board state ${text}`,
      `\n current team: ${this._currentTeam} \n`,
      `team A ships count: ${this._teamAShips.length} \n`,
      `team B ships count: ${this._teamBShips.length} \n`,
      `positions:\n ${positions} \n`
    )
  }
}
