import { ShipState } from './ship.state'

export class ShipStateAttacks extends ShipState {
  public OnEnter(): void {
    super.OnEnter()
  }

  public Update(deltaTime: number): void {
    super.Update(deltaTime)

    if(!this.ship.HasTarget){
      this.ship.StateMachina.Enter(this.ship.StateMachina.IdleState)

      if(!this.ship.Factory.IsLastOneActivated){
        this.ship.Factory.ActivateNext()
      }
    }
  }
}
