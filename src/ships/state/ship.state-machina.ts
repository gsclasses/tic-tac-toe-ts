import { StateMachina } from '@/utils'
import { ShipStateActivated } from './ship.state.activated'
import { ShipStateIdle } from './ship.state.idle'
import { ShipState } from './ship.state'
import { Ship } from '@/ships'
import { ShipStateAttacks } from './ship.state.attacks'
import { ShipStateDead } from './ship.state.dead'
import { ShipStateFlights } from './ship.state.flights'

export class ShipStateMachina extends StateMachina {
  protected states: ShipState[] = []
  protected current: ShipState

  public readonly IdleState: ShipStateIdle
  public readonly ActivatedState: ShipStateActivated
  public readonly MovesState: ShipStateFlights
  public readonly AttacksState: ShipStateAttacks
  public readonly DeadState: ShipStateDead

  constructor(ship: Ship) {
    super()

    this.IdleState = new ShipStateIdle(ship)
    this.ActivatedState = new ShipStateActivated(ship)
    this.MovesState = new ShipStateFlights(ship)
    this.AttacksState = new ShipStateAttacks(ship)
    this.DeadState = new ShipStateDead(ship)

    this.IdleState.Next = [this.ActivatedState, this.DeadState]
    this.ActivatedState.Next = [this.MovesState, this.IdleState]
    this.MovesState.Next = [this.AttacksState, this.IdleState]
    this.AttacksState.Next = [this.IdleState]
    this.DeadState.Next = [this.IdleState]

    this.states = [
      this.IdleState,
      this.ActivatedState,
      this.MovesState,
      this.AttacksState,
      this.DeadState
    ]

    this.current = this.IdleState
  }
}
