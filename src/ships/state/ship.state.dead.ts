import { ShipState } from './ship.state'

export class ShipStateDead extends ShipState {
  public OnEnter(): void {
    super.OnEnter()
    this.ship.Recycle()
  }
}
