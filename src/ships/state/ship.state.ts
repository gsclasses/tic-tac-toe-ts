import { State } from '@/utils'
import { Ship } from '../ship'

export class ShipState extends State {
  constructor(
    protected readonly ship: Ship,
    next?: ShipState[] | null
  ) {
    super(next)
  }
}
