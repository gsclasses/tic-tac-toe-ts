import { ShipState } from './ship.state'

export class ShipStateFlights extends ShipState {

  public OnEnter(): void {
    super.OnEnter()
    this.ship.CanFlight = true
  }

  public OnExit(): void {
    super.OnExit()
    this.ship.CanFlight = false
    // this.ship.Grid.Nodes.map(node => node.Next = null)
  }

  public Update(deltaTime: number): void {
    super.Update(deltaTime)

    // when node has reached it's target we reset it
    const node = this.ship.Node
    const sm = this.ship.StateMachina
    if (node && node.IsTarget) {
      this.ship.Factory.Grid.Nodes.map(node => node.IsTarget = false)

      if (this.ship.HasTarget && sm.CanEnter(sm.AttacksState)) {
        sm.Enter(sm.AttacksState)
      } else {
        sm.Enter(sm.IdleState)

        if (!this.ship.Factory.IsLastOneActivated) {
          this.ship.Factory.ActivateNext()
        }
      }
    }
  }
}
