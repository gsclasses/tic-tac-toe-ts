import { ShipActivatable } from '../components'
import { ShipState } from './ship.state'

export class ShipStateActivated extends ShipState {
  private get component(): ShipActivatable {
    return this.ship.GetComponent(ShipActivatable)
  }

  public OnEnter(): void {
    super.OnEnter()
    this.component.IsActive = true
  }

  public OnExit(): void {
    super.OnExit()
    this.component.IsActive = false
  }

  public Update(deltaTime: number): void {
    super.Update(deltaTime)

    const sm = this.ship.StateMachina
    if ((this.ship.Factory.Grid.Target || this.ship.HasTarget) && sm.CanEnter(sm.MovesState)) {
      sm.Enter(sm.MovesState)
      return
    }
  }
}
