import { Entity, IVector2D, Vector2D, IRange } from '@/utils'
import {
  ShipDrawComponent,
  ShipActivatable,
  ShipHealthComponent,
  ShipDamageComponent,
  ShipTargetComponent,
  ShipDrawAttackComponent,
  ShipFlightComponent,
  ShipFlightAnimatedComponent
} from './components'
import { GridNode } from '@/grid'
import { ShipStateMachina } from './state/ship.state-machina'
import { ShipFactory } from './ship.factory'
import { Settings } from '@/settings'

export class Ship extends Entity {
  private readonly _stateMachina: ShipStateMachina
  private readonly _healthComponent: ShipHealthComponent
  private readonly _flightComponent: ShipFlightComponent
  private readonly _damageComponent: ShipDamageComponent
  private readonly _targetComponent: ShipTargetComponent

  public SpawnNode: GridNode
  public get Health(): ShipHealthComponent {
    return this._healthComponent
  }

  public get StateMachina(): ShipStateMachina {
    return this._stateMachina
  }

  public get Node(): GridNode | null {
    return this._flightComponent.Node
  }

  public set Node(v: GridNode | null) {
    this._flightComponent.Node = v
  }

  public get Position(): IVector2D | null {
    return this._flightComponent.Position
  }

  public get HasTarget(): boolean {
    return !!this.Target
  }

  public get IsDead(): boolean {
    return this._stateMachina.Current === this._stateMachina.DeadState
  }

  public get Target(): Ship | null {
    return this._damageComponent.Target
  }

  public set Target(target: Ship | null) {
    this._damageComponent.Target = target
  }

  public get DamageRange(): IRange {
    return this._damageComponent.Range
  }

  public set CanFlight(v: boolean) {
    this._flightComponent.CanMove = v
  }

  constructor(
    public readonly Factory: ShipFactory,
    public readonly Index: number
  ) {
    super()

    this._stateMachina = new ShipStateMachina(this)

    this._healthComponent = new ShipHealthComponent(Settings.ships.health, Settings.ships.health)
    this._flightComponent = new ShipFlightAnimatedComponent()
    this._damageComponent = new ShipDamageComponent(Settings.ships.damage)
    this._targetComponent = new ShipTargetComponent()
  }

  public Awake(): void {
    this.AddComponent(this._flightComponent)
    this.AddComponent(this._healthComponent)
    this.AddComponent(this._damageComponent)
    this.AddComponent(this._targetComponent)
    this.AddComponent(new ShipActivatable())
    this.AddComponent(new ShipDrawAttackComponent())
    this.AddComponent(new ShipDrawComponent())

    super.Awake()

    this._stateMachina.Start()
  }


  public Update(deltaTime: number): void {
    this.components.map(component => component.Update(deltaTime))
    this._stateMachina.Update(deltaTime)
  }

  public Spawn(node: GridNode): void {
    this.SpawnNode = node
    this._flightComponent.Spawn(node)

    const sm = this.StateMachina
    if (sm.CanEnter(sm.IdleState)) {
      sm.Enter(sm.IdleState)
    }
  }

  public Recycle(): void {
    if (this.Node) {
      this.Node.Ship = null
    }

    this._flightComponent.Recycle()
    this._healthComponent.Recycle()
    this.Factory.Clear()
  }

  public IsWithin(point: IVector2D): boolean {
    if (!this.Position) {
      return false
    }

    const start = new Vector2D(
      this.Position.x - Settings.ships.radius,
      this.Position.y - Settings.ships.radius,
    )

    const end = new Vector2D(
      this.Position.x + Settings.ships.radius,
      this.Position.y + Settings.ships.radius,
    )

    if (point.x < start.x || point.y < start.y) {
      return false
    }

    if (point.x > end.x || point.y > end.y) {
      return false
    }

    return true
  }
}
