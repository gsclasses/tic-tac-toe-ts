import { Team } from '@/game'
import { shipFactoryMockFactory } from './ship-factory.mock'
import { Ship } from './ship'

export const shipMockFactory = (team = Team.B): Ship => shipFactoryMockFactory(team).Produce(0)
