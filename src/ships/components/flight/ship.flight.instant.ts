import { GridNode } from '@/grid'
import { ShipFlightComponent } from './ship.flight'

export class ShipFlightInstantComponent extends ShipFlightComponent {
  public get Node(): GridNode | null {
    return this._node
  }

  public set Node(v: GridNode | null) {
    if(this._node){
      this._node.Ship = null
    }

    this._node = v
    if(this._node){
      this._node.Ship = this.Entity
    }
  }

  public Spawn(node: GridNode): void {
    this._node = node
    this._node.Ship = this.Entity
  }

  public Recycle(): void {
    if(this._node){
      this._node.Ship = null
    }

    this._node = null
  }
}
