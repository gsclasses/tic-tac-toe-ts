import { Component, IVector2D } from '@/utils'
import { GridNode } from '@/grid'
import { Ship } from '@/ships'

export abstract class ShipFlightComponent extends Component {
  public Entity: Ship
  public CanMove = false
  protected _node: GridNode | null

  public abstract get Node(): GridNode | null

  public abstract set Node(v: GridNode | null)

  public get Position(): IVector2D | null {
    return this._node ? this._node.Center : null
  }

  public abstract Spawn(node: GridNode): void

  public abstract Recycle(): void
}
