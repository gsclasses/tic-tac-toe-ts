import { IVector2D, Vector2D } from '@/utils'
import { GridNode } from '@/grid'
import { ShipFlightComponent } from './ship.flight'

export class ShipFlightAnimatedComponent extends ShipFlightComponent {
  private _currentPosition: IVector2D | null = null
  private _startPosition: IVector2D
  private _timeStarted: number
  private _duration = 300;
  private _isMoving = false

  public get Node(): GridNode | null {
    return this._node
  }

  public set Node(v: GridNode | null) {
    throw new Error('Ship Flight Animated Component does not support instant change of node')
  }

  public get Position(): IVector2D | null {
    return this._currentPosition
  }

  public Spawn(node: GridNode): void {
    this._node = node
    this._currentPosition = node.Center
    this._node.Ship = this.Entity
  }

  public Recycle(): void {
    this._currentPosition = null
    this._node = null
  }

  public Update(deltaTime: number): void {
    super.Update(deltaTime)

    if (!this._node) {
      return
    }

    if (!this._node.Next || !this.CanMove) {
      this._isMoving = false
      return
    }

    if (this._isMoving) {
      return this.Flight(this._node, this._node.Next)
    }


    this._isMoving = true
    this._startPosition = this._node.Center
    this._timeStarted = Date.now()
  }

  protected Flight(node: GridNode, next: GridNode): void {
    const targetPosition = next.Center
    const progress = (Date.now() - this._timeStarted) / this._duration
    node.Ship = null
    next.Ship = this.Entity

    if (progress >= 1) {
      this._isMoving = false
      this._currentPosition = targetPosition
      this._node = next

    } else {
      this._currentPosition = Vector2D.Lerp(this._startPosition, targetPosition, progress)
    }
  }
}
