import { Component, GetBestMove } from '@/utils'
import { IBoardMove, Board } from '@/board'
import { ShipTargetComponent, ShipDamageComponent, Ship } from '@/ships'
import { Grid } from '@/grid'
import { Team } from '@/game'

export class ShipActivatable extends Component {
  private isActive = false
  public Entity: Ship

  public get IsActive(): boolean {
    return this.isActive
  }

  public set IsActive(value: boolean) {
    this.isActive = value
    if (!value) {
      this.Entity.Factory.Grid.Nodes.map(node => node.ClearHighlighted())
      this.Entity.Factory.Opposite.Ships.map(ship => ship.GetComponent(ShipTargetComponent).Damage = 0)
      return
    }

    if (!this.Entity.Node) {
      return
    }

    if (this.Entity.Factory.Team === Team.A) {
      setTimeout(() => this.Analyze(), 0)
    } else {
      this.Entity.Node.HighlightVacant()
      this.Entity.Node.HighlightOccupiedByEnemy()
      this.Entity.Factory.Opposite.Ships.map(ship => ship.GetComponent(ShipTargetComponent).Damage = this.Entity.GetComponent(ShipDamageComponent).Range.to)
    }
  }

  private Analyze(): void {
    const team = this.Entity.Factory.Team
    const teamAShips = Board.ConvertFactoryShipsToBoardShips(this.Entity.Factory)
    const teamBShips = Board.ConvertFactoryShipsToBoardShips(this.Entity.Factory.Opposite)
    const current = teamAShips.find(boardShip => boardShip.Index === this.Entity.Index)

    if(!current){
      throw new Error('current must be there')
    }

    const board = new Board(
      teamAShips,
      teamBShips,
      team,
      new Grid(),
      current
    )

    const move = GetBestMove<Team, IBoardMove>(board, team, 3)
    if (!move) {
      return
    }

    if (move.Relocate) {
      const target = this.Entity.Factory.Grid.GetNodeByIndex(move.Relocate)
      if (!target) {
        throw new Error('where is my node??')
      }
      this.Entity.Factory.Grid.DeterminePathTo(target)
      target.IsTarget = true
    } else if (move.Attack) {
      const target = this.Entity.Factory.Opposite.GetShipByIndex(move.Attack.Target.Index)
      if (!target || !target.Node) {
        throw new Error('where is my ship??')
      }

      this.Entity.Target = target
      this.Entity.Factory.Grid.DeterminePathTo(target.Node, true)
    }
  }
}
