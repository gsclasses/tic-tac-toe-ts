import { HoverComponent, IVector2D } from '@/utils'
import { ShipFactory } from '@/ships/ship.factory'
import { ShipTargetComponent } from '../target'

export class ShipFactoryHoverComponent extends HoverComponent {
  public Entity: ShipFactory

  public Hover(on: IVector2D): void {
    for(const ship of this.Entity.Ships){
      let isShown = false
      if (ship.IsWithin(on)) {
        isShown = true
      }

      ship.GetComponent(ShipTargetComponent).IsShown = isShown
    }
  }
}
