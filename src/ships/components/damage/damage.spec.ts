import { ShipDamageComponent } from './damage'
import { IRange } from '@/utils'
import { shipMockFactory } from '@/ships/ship.mock'
import { ShipHealthComponent } from '../health'
import { IDamageSettings } from './damage.h'
import { Ship } from '@/ships'
import { ShipTargetComponent } from '../target'

describe('>>> Ship Damage Component', () => {
  const settings: IDamageSettings = {
    min: [{
      threshold: 10,
      damage: 1
    }, {
      threshold: 30,
      damage: 2
    }, {
      threshold: 50,
      damage: 3
    }, {
      threshold: 100,
      damage: 4
    }],
    max: [{
      threshold: 10,
      damage: 10
    }, {
      threshold: 30,
      damage: 20
    }, {
      threshold: 50,
      damage: 30
    }, {
      threshold: 100,
      damage: 40
    }]
  }

  let comp: ShipDamageComponent
  let entityHealthComp: ShipHealthComponent
  let entity: Ship
  beforeEach(() => {
    entity = shipMockFactory()
    entityHealthComp = new ShipHealthComponent(100, 100)
    entity.AddComponent(entityHealthComp)

    comp = new ShipDamageComponent(settings)
    comp.Entity = entity
  })

  describe('>> range', () => {
    it('should provide access to possible damage based on current health', () => {
      entityHealthComp.Current = 1
      expect(comp.Range).toEqual<IRange>({ from: 1, to: 10 })

      entityHealthComp.Current = 10
      expect(comp.Range).toEqual<IRange>({ from: 1, to: 10 })

      entityHealthComp.Current = 11
      expect(comp.Range).toEqual<IRange>({ from: 2, to: 20 })

      entityHealthComp.Current = 30
      expect(comp.Range).toEqual<IRange>({ from: 2, to: 20 })

      entityHealthComp.Current = 31
      expect(comp.Range).toEqual<IRange>({ from: 3, to: 30 })

      entityHealthComp.Current = 50
      expect(comp.Range).toEqual<IRange>({ from: 3, to: 30 })

      entityHealthComp.Current = 51
      expect(comp.Range).toEqual<IRange>({ from: 4, to: 40 })

      entityHealthComp.Current = 100
      expect(comp.Range).toEqual<IRange>({ from: 4, to: 40 })
    })
  })

  describe('>> make', () => {
    it('should throw error if target ws not yet set', () => {
      expect(() => comp.Make()).toThrow()
    })

    it('should make a damage in range', () => {
      const targetHealth = 100
      const target = shipMockFactory()
      const targetHealthComponent = new ShipHealthComponent(targetHealth, 100)
      target.AddComponent(targetHealthComponent)
      target.AddComponent(new ShipTargetComponent())
      comp.Target = target
      entityHealthComp.Current = 5

      comp.Make()

      expect(targetHealthComponent.Current).toBeGreaterThanOrEqual(targetHealth - comp.Range.to)
      expect(targetHealthComponent.Current).toBeLessThanOrEqual(targetHealth - comp.Range.from)
    })

  })

})
