export interface IDamageSettings {
  min: IDamageSettingsThreshold[]
  max: IDamageSettingsThreshold[]
}

export interface IDamageSettingsThreshold {
  threshold: number
  damage: number
}
