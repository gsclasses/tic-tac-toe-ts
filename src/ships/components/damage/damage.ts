import { Component, random, IRange } from '@/utils'
import { Ship } from '@/ships'
import { ShipHealthComponent } from '../health'
import { IDamageSettings, IDamageSettingsThreshold } from './damage.h'
import { ShipTargetComponent } from '../target'

export class ShipDamageComponent extends Component {
  /** array of threshold for minimum amount of damage range */
  private readonly min: IDamageSettingsThreshold[]

  /** array of threshold for maximum amount of damage range */
  private readonly max: IDamageSettingsThreshold[]

  /** Ref to Entity */
  public Entity: Ship

  /** Ref to Target of damage */
  public Target: Ship | null

  /** What kind of damage can the Entity make? */
  public get Range(): IRange {
    const healthComp = this.Entity.GetComponent(ShipHealthComponent)
    const healthPercent = healthComp.Current / healthComp.Max * 100

    return {
      from: this.getDamageForThreshold(this.min, healthPercent),
      to: this.getDamageForThreshold(this.max, healthPercent)
    }
  }

  constructor(settings: IDamageSettings) {
    super()

    // sort only once
    this.max = settings.max.sort((a, b) => a.threshold - b.threshold)
    this.min = settings.min.sort((a, b) => a.threshold - b.threshold)
  }

  /** Make damage */
  public Make(): void {
    if (!this.Target) {
      throw new Error('Target must be set before making damage')
    }

    this.Target.GetComponent(ShipHealthComponent).Current -= random(this.Range)
    this.Target.GetComponent(ShipTargetComponent).IsShown = false

    setTimeout(() => { this.Target = null }, 0)
  }

  private getDamageForThreshold(thresholds: IDamageSettingsThreshold[], health: number): number {
    const threshold = thresholds.find(item => health <= item.threshold)
    if (!threshold) {
      throw new Error(`threshold not found for settings ${thresholds} and health value ${health}`)
    }

    return threshold.damage
  }
}
