import { IVector2D, IRGBA, Component } from '@/utils'
import { Canvas, CanvasLayer } from '@/game'
import { Ship } from '../../ship'
import { Team } from '@/game'
import { ShipActivatable } from '../activatable/ship.activatable'
import { Settings } from '@/settings'
import { ShipHealthComponent } from '../health'
import { ShipTargetComponent } from '../target'

export class ShipDrawComponent extends Component {
  public Entity: Ship

  private ctx: CanvasRenderingContext2D

  private blinkingDuration = 1000
  private isBlinkingReverse = false
  private blinkingStartedTime: number

  private healthBarDuration = 1500
  private healthBarStartedTime: number
  private lastHealthPercent = 0
  private healthComp: ShipHealthComponent

  private get currentHealthPercent(): number {
    return this.healthComp.Current / this.healthComp.Max
  }

  private get healthToDisplay(): number {
    const targetComponent = this.Entity.GetComponent(ShipTargetComponent)
    if (targetComponent.IsShown) {
      return (this.healthComp.Current - targetComponent.Damage) / this.healthComp.Max
    }

    return this.currentHealthPercent
  }

  private get fillColor(): IRGBA {
    const colors = Settings.ships.colors
    if (this.Entity.GetComponent(ShipActivatable).IsActive) {
      return this.Entity.Factory.Team === Team.A ? colors.active.a : colors.active.b
    }

    return this.Entity.Factory.Team === Team.A ? colors.inactive.a : colors.inactive.b
  }

  private get healthBarColor(): IRGBA {
    return this.Entity.Factory.Team === Team.A ? Settings.ships.colors.health.a : Settings.ships.colors.health.b
  }

  public Awake(): void {
    this.ctx = Canvas.GetCtx(CanvasLayer.Foreground)
    this.healthComp = this.Entity.GetComponent(ShipHealthComponent)
    this.blinkingStartedTime = Date.now()
    this.healthBarStartedTime = Date.now()
  }

  public Update(deltaTime: number): void {
    super.Update(deltaTime)
    if (!this.Entity.Position) {
      return
    }

    this.clearAt(this.Entity.Position)
    if (this.currentHealthPercent <= 0) {
      return
    }

    if (this.currentHealthPercent <= 0.3) {
      this.drawDyingAt(this.Entity.Position, deltaTime)
    } else {
      this.drawAt(this.Entity.Position)
    }

    this.drawHealthAt(this.Entity.Position)

    this.drawDebugAt(this.Entity.Position)
  }

  private clearAt(position: IVector2D): void {
    this.ctx.clearRect(
      position.x - Settings.grid.nodeSize / 2,
      position.y - Settings.grid.nodeSize / 2,
      Settings.grid.nodeSize,
      Settings.grid.nodeSize
    )
  }

  public drawAt(position: IVector2D): void {
    this.ctx.beginPath()
    this.ctx.arc(position.x, position.y, Settings.ships.radius, 0, Math.PI * 2)
    this.ctx.fillStyle = this.fillColor.AsString()
    this.ctx.fill()
  }

  private drawHealthAt(position: IVector2D): void {
    const current = this.healthToDisplay > 0 ? this.healthToDisplay : 0
    if (this.lastHealthPercent !== current) {
      this.healthBarStartedTime = Date.now() - (1 - this.lastHealthPercent) * this.healthBarDuration
      this.lastHealthPercent = current
    }

    let progress = 1 - ((Date.now() - this.healthBarStartedTime) / this.healthBarDuration)
    if (progress < current) {
      progress = current
    }

    const offset = -Math.PI / 2
    const start = 0 + offset
    const end = start + Math.PI * 2 * progress

    this.ctx.beginPath()
    this.ctx.arc(position.x, position.y, Settings.ships.radius, start, end, false)
    this.ctx.strokeStyle = this.healthBarColor.AsString()
    this.ctx.lineWidth = Settings.ships.borderWidth
    this.ctx.stroke()
  }

  private drawDyingAt(position: IVector2D, deltaTime: number): void {
    const r = this.fillColor.R
    const g = this.fillColor.G
    const b = this.fillColor.B
    const a = this.isBlinkingReverse ? 1 - ((Date.now() - this.blinkingStartedTime) / this.blinkingDuration + deltaTime) : (Date.now() - this.blinkingStartedTime + deltaTime) / this.blinkingDuration

    if (a >= 1 || a <= 0) {
      this.blinkingStartedTime = Date.now()
      this.isBlinkingReverse = !this.isBlinkingReverse
    }

    this.ctx.beginPath()
    this.ctx.arc(position.x, position.y, Settings.ships.radius, 0, Math.PI * 2)
    this.ctx.fillStyle = `rgba(${r}, ${g}, ${b}, ${a})`
    this.ctx.fill()
  }

  private drawDebugAt(position: IVector2D): void {
    if (!Settings.debugMode) {
      return
    }

    const targetComponent = this.Entity.GetComponent(ShipTargetComponent)
    this.ctx.font = '14px Arial'
    this.ctx.fillStyle = 'red'
    this.ctx.fillText(`${(this.currentHealthPercent * 100).toFixed()}%, -${targetComponent.Damage} ${this.Entity.Target ? '!' : ''}`, position.x - Settings.ships.radius + 5, position.y + 7)
  }
}
