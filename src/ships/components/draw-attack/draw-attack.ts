import { Component, Vector2D, IVector2D } from '@/utils'
import { Ship } from '@/ships/ship'
import { CanvasLayer, Canvas } from '@/game'
import { Settings } from '@/settings'
import { ShipStateAttacks } from '@/ships'
import { ShipDamageComponent } from '../damage'

export class ShipDrawAttackComponent extends Component {
  public Entity: Ship

  private startedAt: number | null
  private ctx: CanvasRenderingContext2D
  private canvas: HTMLCanvasElement

  private get sourcePosition(): IVector2D | null {
    if(!this.Entity.Node){
      return null
    }

    return new Vector2D(this.Entity.Node.Center.x - this.size / 2, this.Entity.Node.Center.y - this.size / 2)
  }

  private get targetPosition(): IVector2D | null {
    const target = this.Entity.Target
    if (!target || !target.Node) {
      return null
    }

    if (!(this.Entity.StateMachina.Current instanceof ShipStateAttacks)) {
      return null
    }

    if (!this.startedAt) {
      this.startedAt = Date.now()
    }

    return new Vector2D(target.Node.Center.x - this.size / 2, target.Node.Center.y - this.size / 2)
  }

  constructor(
    private readonly duration = 500,
    private readonly size = 10
  ) {
    super()
  }

  public Awake(): void {
    this.canvas = Canvas.GetInstance(CanvasLayer.Middle)
    this.ctx = Canvas.GetCtx(CanvasLayer.Middle)
  }

  public Update(deltaTime: number): void {
    super.Update(deltaTime)

    if (!this.targetPosition || !this.startedAt || !this.sourcePosition) {
      return
    }

    const progress = (Date.now() - this.startedAt) / this.duration
    if (progress >= 1) {
      this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
      this.startedAt = null

      this.Entity.GetComponent(ShipDamageComponent).Make()

      return
    }

    const position = Vector2D.Lerp(this.sourcePosition, this.targetPosition, progress)

    this.ctx.clearRect(position.x, position.y, this.size, this.size)

    this.ctx.beginPath()
    this.ctx.rect(position.x, position.y, this.size, this.size)
    this.ctx.closePath()
    this.ctx.fillStyle = Settings.ships.colors.projectile.AsString()
    this.ctx.fill()
  }
}
