import { Component } from '@/utils'
import { Ship } from '../../ship'

export class ShipHealthComponent extends Component {
  /** Entity ref */
  public Entity: Ship

  /** Current amount of health */
  private current: number

  /** Current health getter */
  public get Current(): number {
    return this.current
  }

  /** Current health setter */
  public set Current(v: number) {
    if (v < 0) {
      v = 0
    } else if (v > this.Max) {
      v = this.Max
    }

    if (v === this.current) {
      return
    }

    this.current = v
  }

  constructor(
    current: number,
    public readonly Max: number
  ) {
    super()
    this.current = current
  }

  public Update(deltaTime: number): void {
    super.Update(deltaTime)

    const sm = this.Entity.StateMachina
    if(this.Current <= 0 && sm.CanEnter(sm.DeadState)){
      sm.Enter(sm.DeadState)
      this.Recycle()
    }
  }

  public Recycle(): void {
    this.current = this.Max
  }
}
