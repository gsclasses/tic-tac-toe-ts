import { ShipHealthComponent } from './ship.health'
import { Component } from '@/utils'
import { ShipDrawComponent } from '../draw'
import { shipMockFactory } from '@/ships/ship.mock'

describe('>>> Ship Health Component', () => {
  const entity = shipMockFactory()
  const drawable = new ShipDrawComponent()
  const max = 100
  let comp: ShipHealthComponent

  beforeEach(() => {
    comp = new ShipHealthComponent(max, max)

    entity.AddComponent(drawable)
    entity.AddComponent(comp)
  })

  it('should be component', () => {
    expect(comp).toBeInstanceOf(Component)
  })

  it('should get initial health value from constructor', () => {
    expect(comp.Max).toEqual(max)
    expect(comp.Current).toEqual(max)
  })

  it('should allow only change in current health between 0 and Max', () => {
    comp.Current = comp.Max + 1
    expect(comp.Current).toEqual(comp.Max)

    comp.Current = -1
    expect(comp.Current).toEqual(0)
  })
})
