import { Component } from '@/utils'
import { Ship } from '@/ships/ship'

export class ShipTargetComponent extends Component {
  public Entity: Ship
  public Damage = 0
  public IsShown = false
}
