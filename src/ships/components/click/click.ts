import { IVector2D, ClickComponent } from '@/utils'
import { ShipFactory } from '@/ships'

export class ShipFactoryClickComponent extends ClickComponent {
  /** Ref to entity */
  public Entity: ShipFactory

  public Show = true

  public Click(on: IVector2D): void {
    for (const ship of this.Entity.Ships) {
      if (ship.IsWithin(on) && ship.Node && ship.Node.HasEnemy) {
        const factory = this.Entity.Opposite
        if (factory.Current) {
          factory.Current.Target = ship
          this.Entity.Grid.DeterminePathTo(ship.Node, true)
        }
      }
    }
  }
}
