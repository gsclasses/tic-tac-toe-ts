import { Ship } from './ship'
import { Team } from '@/game'
import { Entity } from '@/utils'
import { Settings } from '@/settings'
import { Grid, GridNode } from '@/grid'
import {
  ShipFactoryHoverComponent,
  ShipFactoryClickComponent
} from './components'

export enum ShipType {
  Regular,
  Simplified
}
export class ShipFactory extends Entity {
  /* List of the Ships */
  private _ships: Ship[] = []

  /* Ref to the Ship that was the last one being activated  */
  private _index = 0

  /*  Verifies that all ships produced by this factory are ready to go */
  private _isReady = false

  /*  Ref to opposite team's factory  */
  private _opposite: ShipFactory | null = null

  /*  Determines if it is the Team's, this factory belongs to, current turn  */
  public IsCurrent = false

  /**
   * Ships getter
   */
  public get Ships(): Ship[] {
    return this._ships.filter(ship => !ship.IsDead)
  }

  /**
   * isReady getter
   */
  public get IsReady(): boolean {
    return this._isReady
  }

  public get Grid(): Grid {
    return this._grid
  }

  public get Opposite(): ShipFactory {
    const opposite = this._opposite
    if (!opposite) {
      throw new Error('Opposite factory not set')
    }

    return opposite
  }

  public set Opposite(v: ShipFactory) {
    const opposite = this._opposite
    if (opposite) { // do not allow resetting of the Opposite
      throw new Error('Opposite factory already set')
    }

    this._opposite = v
  }

  /* Returns most recently activated ship */
  public get Current(): Ship | null {
    return this.IsCurrent ? this.Ships[this._index - 1] : null
  }

  public get IsLastOneActivated(): boolean {
    return this._index === this.Ships.length
  }

  public get Health(): number {
    const health = { current: 0, max: 0 }
    for (const ship of this._ships) {
      health.max += ship.Health.Max
      health.current += ship.Health.Current
    }

    const result = Math.round((health.current / health.max) * 100)

    return isNaN(result) ? 0 : result
  }

  constructor(
    public readonly Team: Team,
    private readonly _grid: Grid

  ) {

    super()
    for (let i = 0; i < Settings.ships.fleetSize; i++) {
      this._ships.push(this.Produce(i))
    }
  }

  /**
   * When Entity Awakens
   */
  public Awake(): void {
    super.Awake()

    if (this.Team === Team.A) {
      this.AddComponent(new ShipFactoryHoverComponent())
      this.AddComponent(new ShipFactoryClickComponent())
    }
  }

  public Produce(index: number): Ship {
    return new Ship(this, index)
  }

  public Spawn(): void {
    let i = 2
    for (const ship of this._ships) {
      ship.Spawn(this.DetermineNode(i))
      i++
    }
  }

  public Clear(): void {
    this._index = 0
  }

  public RecycleAll(): void {
    for (const ship of this._ships) {
      ship.Recycle()
      if (ship.StateMachina.CanEnter(ship.StateMachina.IdleState)) {
        ship.StateMachina.Enter(ship.StateMachina.IdleState)
      }
    }
  }

  /**
   * Update Loop
   */
  public Update(deltaTime: number): void {
    this.Ships.map(ship => ship.Update(deltaTime))

    if (this._isReady) {
      return
    }

    if (!this.Ships.some(ship => !ship.StateMachina.IsStarted)) {
      this._isReady = true
    }
  }

  public ActivateNext(): void {
    if (this._index === this.Ships.length) {
      this._index = 0
    }

    this._index++
    if (this.Current && this.Current.StateMachina.CanEnter(this.Current.StateMachina.ActivatedState)) {
      this.Current.StateMachina.Enter(this.Current.StateMachina.ActivatedState)
    }
  }

  public GetShipByIndex(index: number): Ship | undefined {
    return this._ships[index]
  }

  public Clone(grid: Grid): ShipFactory {
    const clone = new ShipFactory(this.Team, grid)
    clone._index = this._index
    for (const ship of clone._ships) {
      ship.Awake()

      const old = this.GetShipByIndex(ship.Index)
      if (!old || !old.Node || !old.Node.Index) {
        throw new Error('Cannot find old ship by index while cloning factory')
      }

      const node = grid.GetNodeByIndex(old.Node.Index)
      if (!node) {
        throw new Error('Cannot find old ship by index while cloning factory')
      }

      ship.Health.Current = old.Health.Current
      ship.Spawn(node)
    }

    return clone
  }

  /**
   * Set Ships on left or right side of the Gris depending on the Team
   */
  private DetermineNode(i: number): GridNode {
    return this.Grid.Nodes[this.Team == Team.B ? i * Settings.grid.dimension : this.Grid.Nodes.length - 1 - i * Settings.grid.dimension]
  }
}
