import { ShipFactory } from './ship.factory'
import { Team } from '@/game'
import { Grid } from '@/grid'

export const shipFactoryMockFactory = (team = Team.B): ShipFactory => new ShipFactory(team, new Grid())
