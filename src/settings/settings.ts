import { RGBA } from '@/utils/colors'

export const Settings = Object.freeze({
  grid: {
    dimension: 6,
    nodeSize: 100,
    nodeOffset: 10,
    accessible: new RGBA(176, 190, 197, 1),
    accessibleWithEnemy: new RGBA(255, 205, 210, 1),
    default: new RGBA(245, 245, 245, 1)
  },

  ships: {
    health: 100,
    radius: 40,
    flightRange: 3,
    borderWidth: 2,
    fleetSize: 3,
    colors: {
      inactive: {
        a: new RGBA(187, 222, 251, 1),
        b: new RGBA(255, 236, 179, 1)
      },
      active: {
        a: new RGBA(79, 195, 247, 1),
        b: new RGBA(255, 213, 79, 1)
      },
      health: {
        a: new RGBA(1, 87, 155, 1),
        b: new RGBA(33, 33, 33, 1)
      },
      projectile: new RGBA(245, 127, 23, 1)
    },
    damage: {
      min: [{
        threshold: 10,
        damage: 1
      }, {
        threshold: 30,
        damage: 3
      }, {
        threshold: 50,
        damage: 5
      }, {
        threshold: 100,
        damage: 10
      }],
      max: [{
        threshold: 10,
        damage: 5
      }, {
        threshold: 30,
        damage: 8
      }, {
        threshold: 50,
        damage: 10
      }, {
        threshold: 100,
        damage: 13
      }]
    }
  },

  debugMode: true
})
