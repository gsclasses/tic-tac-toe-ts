import { IVector2D, ClickComponent } from '@/utils'
import { Grid } from './grid'
import { GridNode } from './grid.node'
import { GridNodeAccessibleComponent } from './components'

export class GridClickable extends ClickComponent {
  /**
   * Ref to Entity
   */
  public Entity: Grid

  public Click(point: IVector2D): void {
    let clicked: GridNode | null = null

    for (const node of this.Entity.Nodes) {
      if (!node.IsWithin(point)) {
        continue
      }

      if (!node.GetComponent(GridNodeAccessibleComponent).IsAccessible) {
        return
      }

      clicked = node
    }

    if (!clicked) {
      return
    }

    this.Entity.Nodes.map(node => node.GetComponent(GridNodeAccessibleComponent).Clear())
    this.Entity.DeterminePathTo(clicked)
    clicked.IsTarget = true
  }
}
