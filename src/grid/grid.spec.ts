import { Grid } from './grid'

describe('>>> Grid', () => {
  let grid: Grid
  beforeEach(() => {
    grid = new Grid()
  })

  it('should exist', () => {
    expect(grid).toBeDefined()
  })
})
