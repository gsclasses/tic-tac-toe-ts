import { Vector2D, IVector2D, Entity } from '@/utils'
import { Settings } from '@/settings'
import { GridNodeOccupiedComponent, GridNodeAccessibleComponent, GridNodeDrawComponent } from './components'
import { Ship } from '@/ships'

export class GridNode extends Entity {
  private readonly _gridNodeDrawComponent: GridNodeDrawComponent
  private readonly _gridNodeAccessibleComponent: GridNodeAccessibleComponent
  private readonly _gridNodeOccupiedComponent: GridNodeOccupiedComponent
  public Ship: Ship | null = null
  public Next: GridNode | null = null
  public IsTarget = false

  public get Width(): number {
    return this.End.x - this.Start.x
  }

  public get Height(): number {
    return this.End.y - this.Start.y
  }

  /** Calculates the position of the center point of the node */
  public get Center(): IVector2D {
    return new Vector2D(
      this.Start.x + this.Width / 2,
      this.Start.y + this.Height / 2
    )
  }

  public get IsBlockedByCurrent(): boolean {
    return this.Ship ? (this.Ship.Factory.Current === this.Ship) : false
  }

  public get HasEnemy(): boolean {
    return this._gridNodeOccupiedComponent.IsEnemyDetected
  }

  constructor(
    public readonly Start: IVector2D,
    public readonly End: IVector2D,
    public readonly Index: IVector2D,
    public readonly Neighbors: GridNode[]
  ) {
    super()

    this._gridNodeDrawComponent = new GridNodeDrawComponent()
    this._gridNodeAccessibleComponent = new GridNodeAccessibleComponent()
    this._gridNodeOccupiedComponent = new GridNodeOccupiedComponent()
  }

  /** When Entity Awakens */
  public Awake(): void {
    this.AddComponent(this._gridNodeDrawComponent)
    this.AddComponent(this._gridNodeAccessibleComponent)
    this.AddComponent(this._gridNodeOccupiedComponent)

    super.Awake()
  }

  public IsWithin(point: IVector2D): boolean {
    if (point.x < this.Start.x) {
      return false
    }

    if (point.x > this.End.x) {
      return false
    }

    if (point.y < this.Start.y) {
      return false
    }

    if (point.y > this.End.y) {
      return false
    }

    return true
  }

  public HighlightVacant(): void {
    this._gridNodeAccessibleComponent.Search(Settings.ships.flightRange)
  }

  public HighlightOccupiedByEnemy(): void {
    if (!this.Ship) {
      throw new Error('Cannot find occupied nodes for inexistent ship')
    }

    this._gridNodeOccupiedComponent.Search(this.Ship, Settings.ships.flightRange)
  }

  public ClearHighlighted(): void {
    this._gridNodeAccessibleComponent.Clear()
    this._gridNodeOccupiedComponent.Clear()
  }
}
