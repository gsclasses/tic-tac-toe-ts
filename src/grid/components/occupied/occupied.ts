import { Component } from '@/utils'
import { GridNode } from '../../grid.node'
import { Ship } from '@/ships'

export class GridNodeOccupiedComponent extends Component {
  private isEnemyDetected = false

  /** Ref to Entity */
  public Entity: GridNode

  /** Determines whether to highlight or not this node  */
  public readonly Show = true

  public get IsEnemyDetected (): boolean {
    return this.isEnemyDetected
  }

  public Search(ship: Ship, range: number): void {
    if(this.Entity.Ship && this.Entity.Ship.Factory.Team !== ship.Factory.Team){
      this.isEnemyDetected = true
    }

    const newRange = --range
    if(newRange <= 0){
      return
    }

    this.Entity.Neighbors
      // .filter(neighbor => neighbor.Ship)
      .map(neighbor => neighbor.GetComponent(GridNodeOccupiedComponent).Search(ship, range))
  }

  public Clear(): void {
    this.isEnemyDetected = false
  }
}
