import { Component } from '@/utils'
import { GridNode } from '@/grid'

export class GridNodeAccessibleComponent extends Component {
  private isAccessible = false

  /** Ref to Entity */
  public Entity: GridNode

  /** Determines whether to highlight or not this node  */
  public readonly Show = true

  public get IsAccessible(): boolean {
    return this.isAccessible
  }

  public Search(range: number): void {
    if(!this.Entity.Ship){
      this.isAccessible = true
    }


    const newRange = --range
    if (newRange <= 0) {
      return
    }

    this.Entity.Neighbors
      .filter(neighbor => !neighbor.Ship)
      .map(neighbor => neighbor.GetComponent(GridNodeAccessibleComponent).Search(range))
  }

  public Clear(): void {
    this.isAccessible = false
  }
}
