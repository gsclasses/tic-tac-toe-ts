import { Component } from '@/utils'
import { GridNode } from '../../grid.node'
import { Canvas, CanvasLayer } from '@/game'
import { Settings } from '@/settings'
import { GridNodeAccessibleComponent } from '../accessible'
import { GridNodeOccupiedComponent } from '../occupied'

export class GridNodeDrawComponent extends Component {
  /** Canvas ref */
  private ctx: CanvasRenderingContext2D

  /** Determine the color of the node */
  private get color(): string {
    if (this.Entity.GetComponent(GridNodeAccessibleComponent).IsAccessible) {
      return Settings.grid.accessible.AsString()
    }

    const enemyDetectComp = this.Entity.GetComponent(GridNodeOccupiedComponent)
    if (enemyDetectComp.IsEnemyDetected && enemyDetectComp.Show) {
      return Settings.grid.accessibleWithEnemy.AsString()
    }

    return Settings.grid.default.AsString()
  }

  /**
   * Ref to Entity
   */
  public Entity: GridNode

  public Awake(): void {
    super.Awake()

    this.ctx = Canvas.GetCtx(CanvasLayer.Background)
  }

  public Update(deltaTime: number): void {
    super.Update(deltaTime)

    this.clear()
    this.draw()
    this.drawDebugInfo()
  }

  private clear(): void {
    this.ctx.clearRect(this.Entity.Start.x, this.Entity.Start.y, this.Entity.Width, this.Entity.Height)
  }

  /**
   * Draws the node
   */
  private draw(): void {
    this.ctx.beginPath()
    this.ctx.fillStyle = this.color
    this.ctx.rect(this.Entity.Start.x, this.Entity.Start.y, this.Entity.Width, this.Entity.Height)
    this.ctx.fill()
  }

  private drawDebugInfo(): void {
    if (!Settings.debugMode) {
      return
    }

    const entity = this.Entity
    this.ctx.font = '14px Arial'
    this.ctx.fillStyle = 'red'
    this.ctx.fillText(entity.Index.AsString(), entity.Start.x, entity.Start.y + 14)

    if (entity.Next) {
      const nextPosition = entity.Next.Index
      const position = entity.Index
      let text = ''
      if (nextPosition.x === position.x) {
        if (nextPosition.y > entity.Index.y) {
          text = 'V'
        } else {
          text = '^'
        }
      } else if (nextPosition.y === entity.Index.y) {
        if (nextPosition.x > entity.Index.x) {
          text = '>'
        } else {
          text = '<'
        }
      } else {
        throw new Error('Next is wrong')
      }

      this.ctx.fillText(text, entity.Start.x + entity.Width / 2 - 7, entity.Start.y + entity.Height / 2 + 7)
    }

    this.ctx.fillText(`${this.Entity.IsTarget ? 'T' : ''}`, entity.Start.x, entity.Start.y + entity.Height - 1)

    if (this.Entity.Ship) {
      this.ctx.fillStyle = this.Entity.IsBlockedByCurrent ? 'green' : 'red'
      this.ctx.fillText('X', entity.Start.x + entity.Width - 14, entity.Start.y + 14)
    }
  }
}
