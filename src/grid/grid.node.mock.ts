import { GridNode } from './grid.node'
import { Vector2D } from '@/utils'

export const gridNodeMockFactory = (
  start = new Vector2D(0, 0),
  end = new Vector2D(1, 1),
  index = new Vector2D(1, 1),
  neighbors: GridNode[] = []
): GridNode => new GridNode(start, end, index, neighbors)
