import { GridClickable } from './grid.clickable'
import { GridNode } from './grid.node'
import { Entity, IUpdatable, Vector2D, IVector2D, IGraph, BreadthFirstSearch } from '@/utils'
import { Settings } from '@/settings'

export class Grid extends Entity implements IUpdatable, IGraph {
  public Nodes: GridNode[] = []

  public get Target(): GridNode | undefined {
    return this.Nodes.find(node => node.IsTarget)
  }

  constructor() {
    super()

    for (let y = 0; y < Settings.grid.dimension; y++) {
      for (let x = 0; x < Settings.grid.dimension; x++) {
        const start = new Vector2D(
          x * (Settings.grid.nodeSize + Settings.grid.nodeOffset) + Settings.grid.nodeOffset,
          y * (Settings.grid.nodeSize + Settings.grid.nodeOffset) + Settings.grid.nodeOffset
        )

        const end = new Vector2D(
          start.x + Settings.grid.nodeSize,
          start.y + Settings.grid.nodeSize
        )

        const index = new Vector2D(x, y)

        const top = this.Nodes.find(node => node.Index.x === index.x && node.Index.y === index.y - 1)
        const left = this.Nodes.find(node => node.Index.x === index.x - 1 && node.Index.y === index.y)

        const neighbors: GridNode[] = []
        const node = new GridNode(start, end, index, neighbors)

        this.Nodes.push(node)

        if (left) {
          neighbors.push(left)
          left.Neighbors.push(node)
        }

        if (top) {
          neighbors.push(top)
          top.Neighbors.push(node)
        }
      }
    }
  }

  public Awake(): void {
    this.AddComponent(new GridClickable())
  }

  public Update(deltaTime: number): void {
    super.Update(deltaTime)
    this.Nodes.map(node => node.Update(deltaTime))
  }

  public GetNodeByIndex(index: IVector2D): GridNode | undefined {
    return this.Nodes.find(node => node.Index.x === index.x && node.Index.y === index.y)
  }

  public GetNeighborsForNodeWithIndex(index: IVector2D): IVector2D[] {
    const node = this.GetNodeByIndex(index)
    if (!node) {
      throw new Error(`Node with index ${index.AsString()} does not exist`)
    }

    const nodes = []
    for (const neighbor of node.Neighbors) {
      if (!neighbor.Ship || neighbor.IsBlockedByCurrent) {
        nodes.push(neighbor.Index)
      }
    }

    return nodes
  }

  public Clear(): void {
    this.Nodes.map(node => {
      node.Next = null
      node.Ship = null
      node.IsTarget = false
    })
  }

  public DeterminePathTo(node: GridNode, toAttack = false): void {
    const path = BreadthFirstSearch(this, node.Index)

    for (const i in path) {
      if (Object.prototype.hasOwnProperty.call(path, i)) {
        const current = this.GetNodeByIndex(Vector2D.GetFromString(i))
        if (!current) {
          throw new Error(`Node with the index ${i} not found`)
        }

        const v = path[i]
        if (!v) {
          current.Next = null
          continue
        }

        const next = this.GetNodeByIndex(v)
        if (!next) {
          throw new Error(`Node with the index ${v} not found`)
        }

        if (toAttack) {
          if (next === node) {
            current.IsTarget = true
          }
        }

        current.Next = next
      }
    }
  }
}
